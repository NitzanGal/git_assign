#include "Vector.h"
#include <iostream>

using namespace std;

Vector::Vector(int n) 
{
	// illegal parameter value
	if (n < 2)
	{
		n = 2;
	}

	_size = 0;

	_capacity = n;
	_resizeFactor = n;

	this->_elements = new int[this->_resizeFactor];
}

Vector::Vector(const Vector& other) : _elements(NULL)
{
	_size = other.size();
	_capacity = other.capacity();
	_resizeFactor = other.resizeFactor();

	this->_elements = new int[other.capacity()];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}


Vector :: ~Vector()
{
	if (this->_elements)
	{
		delete[] this->_elements;
	}

	this->_elements = NULL;
	this->_size = 0;
	this->_capacity = 0;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return this->_size == 0;
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}




void Vector::push_back(const int& val)
{
	// array is full
	if (this->_size == this->_capacity)
	{
		this->reserve(this->_resizeFactor + this->_capacity);
	}

	// add the value at the end of free space
	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	if (_size > 0)
	{
		this->_size--;
		return _elements[_size];
	}
	else
	{
		cout << "error: pop from empty vector" << endl;
		return -9999;
	}
}

void Vector::reserve(int n)
{
	// if not enough capacity in the vector
	if (n > this->_capacity)
	{
		//save old values
		int* temp = this->_elements;

		//calculatation of next allocation
		int cp = this->_capacity + this->_resizeFactor;
		while (n > cp)
		{
			cp += _resizeFactor;
		}

		//create new array
		this->_elements = new int[cp];
		this->_capacity = cp;

		//copy the old values
		for (int i = 0; i < this->_size; i++)
			this->_elements[i] = temp[i];

		//free the old array
		delete[] temp;
	}

}



void Vector::resize(int n)
{
	this->reserve(n);
	_size = n;
}

void Vector::resize(int n, const int& val)
{
	int i;
	int start = _size;

	this->resize(n);

	for (i = start; i < _size; ++i)
	{
		this->_elements[i] = val;
	}
}



int& Vector :: operator[](int n) const
{
	if (n >= this->_size || n < 0)
	{
		cout << "ERROR - index out of range" << endl;
		n = 0;
	}

	return this->_elements[n];
}



Vector& Vector :: operator=(const Vector& other)
{
	// clear this vector
	delete[] _elements;
	
	// copy fields
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	
	// allocating memory 
	this->_elements = new int[this->_capacity];
	
	// copy all the elements
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}